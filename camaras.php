<?php
// Check if the form is submitted 
$logged = $_REQUEST['logged'];
if( $logged !== "true" )
{ 
    header('Location: login.html');
    die();
}
?>
<!DOCTYPE html> 
<html lang="es">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css"> 
<link rel="stylesheet" href="css/css_camaras.css">
 
<link rel="icon" href="img/favicon.png" sizes="32x32" /> 

<script src="js/jquery.min.js"></script>
<script src="js/moment.min.js"></script> 

<script type="text/javascript" src="js/jquery.maphilight.js"></script>
<script type="text/javascript" src="js/funcionesCamaras.js"></script>  
<style>
	body {
		overflow: hidden;
	}
	.map-container {
		height: 100%;
		width: 100%;
		margin-top: 0px;
		z-index: 0;
		display: block !important;
	}
	.btn-xl {
    padding: 10px 20px;
    font-size: 1.75em;
    border-radius: 10px;
} 
</style>
</head>
   <body> 
    <center>
    <div style="margin-top: 25px;">
    <button type="button" disabled class="btn btn-secondary">ALMACENES</button>
    <div class="btn-group  ">
                <button type="button" class="btn btn-info dropdown-toggle  " data-toggle="dropdown" aria-expanded="false">
                CAMIONES
                </button>
                <div class="dropdown-menu ">
                    <a class="dropdown-item" href="camiones.php?logged=<?php echo $logged ?>">Histórico Rutas</a>
                    <a class="dropdown-item" href="recomendacionRutaCamiones.php?logged=<?php echo $logged ?>">Ruta recomendada</a> 
                </div>
            </div>
   
    <a href="historicos.php?logged=<?php echo $logged ?>"><button type="button" class="btn btn-info">HISTÓRICOS</button></a> 

    <a href="login.html" class="btn btn-warning">
                <span class="glyphicon glyphicon-log-out"></span> Salir
            </a>
    </div>
</center>
 
    <div class="col-5" id="div_alertas" style="left: 15px;position:absolute;bottom:0px;border-radius: 15px 15px 0px 0px;color: #fff;z-index: 99999; background-color:rgba(0,0,0,0.6);"> 
            <center><span id="titulo_alertas"><u>ALERTAS</u></span>
            <table id="tabla_alertas" style="display:none;border-spacing: 10px;border-collapse: separate;"> 
            </table>
            </center>
        
        
    </div>
    <div class="col-1"></div>
    <div class="col-5 div_anomalias" id="div_anomalias" style="position:absolute;bottom:0px;right: 15px;border-radius: 15px 15px 0px 0px;color: #fff;z-index: 99999; background-color:rgba(0,0,0,0.6);"> 
            <center><span id="titulo_anomalias"><u>ANOMALÍAS</u></span> 
            <table id="tabla_anomalias" style="display:none;border-spacing: 10px;border-collapse: separate;"> 
            </table>
            </center>  
    </div> 

 

<MAP NAME="map1">
    <AREA HREF="acceso.php?camara='Bitempera 6&logged=<?php echo $logged ?>" ALT="Bitempera 6" TITLE="Acceso a Bitempera 6" SHAPE=RECT COORDS="5,130,298,579">
    <AREA HREF="acceso.php?camara='Bitempera 5&logged=<?php echo $logged ?>" ALT="Bitempera 5" TITLE="Acceso a Bitempera 5" SHAPE=RECT COORDS="300,130,665,579">
    <AREA HREF="acceso.php?camara='Congelados&logged=<?php echo $logged ?>" ALT="Congelados" TITLE="Acceso a Congelados" SHAPE=RECT COORDS="676,300,1000,579"> 
    <AREA HREF="acceso.php?camara='Frescos 2&logged=<?php echo $logged ?>" ALT="Frescos 2" TITLE="Acceso a Frescos 2" SHAPE=RECT COORDS="1002,300,1310,579"> 
    <AREA HREF="acceso.php?camara='Bitempera 4&logged=<?php echo $logged ?>" ALT="Bitempera 4" TITLE="Acceso a Bitempera 4" SHAPE=RECT COORDS="748,14,1013,300">  
    <AREA HREF="acceso.php?camara='Muelle&logged=<?php echo $logged ?>" ALT="Muelle" TITLE="Acceso a Muelle" SHAPE=RECT COORDS="1313,14,1390,579"> 
</MAP> 
<div USEMAP="#map1" id="main_image" class="absoluteCenter"  > 
<IMG id="main_image" class="map" SRC="img/main.png"
   ALT="map of GH site" BORDER=0 WIDTH=1400px HEIGHT=600px
   USEMAP="#map1"> 
</div> 
<script>
var alertasMostradas=false;
var anomaliasMostradas=false;
var atendiendo = false;
$('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
$(window).on('load', function(){
  setTimeout(removeLoader, 2000); 
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(500, function() { 
      $( "#loadingDiv" ).remove(); 
  });  
}

$( document ).ready(function() { 
     
    var isMobile = false;
 // device detection
 if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
 || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
 isMobile = true;
} 

if( isMobile == true )
    {
        $(':button').addClass('btn-xl'); 
        $('.btn-sm').addClass('btn-xl');  
        $('.dropdown-item').addClass('btn-xl');
		$('#select_vehiculo').addClass('btn-xl');  
		$('#select_id_ruta').addClass('btn-xl');  
		$('#div_info').css('font-size','20px');  
		$('#div_anomalias').css('bottom','-20px');
		 
         
        $('#main_image').remove();  

    }
    
    $( "#btn_atender" ).click(function() {
        if( $( "#btn_atender" ).hasClass( "todas" ))
        {
            $( "#btn_atender" ).html( "Sin atender" );
            $( "#btn_atender" ).removeClass("todas");
            $( '.atendida' ).hide(); 
        }
        else
        {
            $( "#btn_atender" ).html( "Todas" );
            $( "#btn_atender" ).addClass("todas");
            $( '.atendida' ).show();
        }
    }); 
    setTimeout(pollingAlarmas, 1000);
});
function pollingAlarmas()
{ 
     if( atendiendo == true )
     {
        setTimeout(pollingAlarmas, 10000); 
        return false;
     }
     

    if(anomaliasMostradas==false) $('#tabla_anomalias').empty();
    if(alertasMostradas==false)$('#tabla_alertas').empty();
    
    $.ajax({
            url: "rest/cargaAlertas.php",
            type: "post",
            dataType: "json" ,
            success: function (alertas) { 
                var hayAlertas = false;
                var hayAnomalias = false;
                $.each( alertas, function( index, alerta ){  
                    var atendida = alerta.atendida;
                    var txtAtendida = "";
                    var aEnviar = "";
                    var claseBoton = "";
                    if( atendida == 0 )
                    {
                        atendida="sin_atender";
                        txtAtendida = "Sin atender";
                        aEnviar = 1;
                        claseBoton="danger";
                    }
                    else
                    {
                        atendida="atendida";
                        txtAtendida = "Atendida";
                        aEnviar = 0;
                        claseBoton="success";
                    }
                   
                    if( alerta.tipo === "alerta" && alertasMostradas == false )
                    {
                        $('#tabla_alertas').empty();
                        var fila = "<tr><td>* " + alerta.descripcion + "</td></tr>";
                        $('#tabla_alertas').append( fila ); 
                        hayAlertas=true; 
                    }
                    else if( alerta.tipo === "anomalia"  && anomaliasMostradas == false )
                    {
                        if( anomaliasMostradas == true )return false;
                        $('#tabla_anomalias').empty();
                        var fila = "";  
                        if( alerta.mas != "" && alerta.mas != null )
                        { 
                            var arrMsgExtra = alerta.mas;
                            arrMsgExtra = arrMsgExtra.replace("[","");
                            arrMsgExtra = arrMsgExtra.replace("]","");
                            arrMsgExtra = arrMsgExtra.split(","); 
                            console.log(arrMsgExtra.length);  
                            fila = "<tr class='" + atendida + "'><td>* " + alerta.descripcion;
                            for (i = 0; i < arrMsgExtra.length; ++i) { 
                                fila = fila + "<br>&nbsp;&nbsp;&nbsp;&nbsp;- " + arrMsgExtra[i].replace(/\"/g,' ');;
                            }   
                            fila = fila + "</td><td></td><td style='vertical-align:top;'><button  onclick='atenderAnomalia(" + alerta.id + " , " + aEnviar + ")' type='button' class='btn-sm btn btn-" + claseBoton + "'>" + txtAtendida + "</button>";

                        }
                        else
                        {
                            fila = "<tr class='" + atendida + "'><td>* " + alerta.descripcion + "</td></td><td></td><td><button  onclick='atenderAnomalia(" + alerta.id + " , " + aEnviar + ")' type='button' class='btn-sm btn btn-" + claseBoton + "'>" + txtAtendida + "</button></td></tr>";
                        } 
                        $('#tabla_anomalias').append( fila );  
                        hayAnomalias=true; 
                    }

                    if( alertas.length == index+1 )
                    {
                        if( hayAlertas == true )
                        { 
                            $('#titulo_alertas').addClass('parpadea'); 
                            $('#div_alertas').on('mouseover', show_div_alertas);
                            $('#div_alertas').on('mouseleave', hide_div_alertas);
                        }
                        else
                        {
                            $('#titulo_alertas').removeClass('parpadea'); 
                        }
                        if( hayAnomalias == true )
                        { 
                            $('#titulo_anomalias').addClass('parpadea');  
                            $('#div_anomalias').on('mouseover', show_div_anomalias);
                            $('#div_anomalias').on('mouseleave', hide_div_anomalias);
                        }
                        else
                        {
                            $('#titulo_anomalias').removeClass('parpadea'); 
                        }
                    }
                     
                });  
                setTimeout(pollingAlarmas, 10000); 
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
}
function show_div_alertas()
{ 
    $('#tabla_alertas').show();
    alertasMostradas=true;
}
function hide_div_alertas()
{ 
    $('#tabla_alertas').hide();
    alertasMostradas=false;
}
function show_div_anomalias()
{  
    $('#tabla_anomalias').show();
    anomaliasMostradas=true;
}
function hide_div_anomalias()
{ 
    $('#tabla_anomalias').hide();
    anomaliasMostradas=false;
}
function atenderAnomalia(idAlerta,aEnviar)
{  
    atendiendo = true;
    $.ajax({
        url: "rest/atiendeAlerta.php?idAlerta="+idAlerta+"&aEnviar="+aEnviar,
        type: "post", 
        dataType: "HTML",
        success: function () {  
            $('#tabla_anomalias').hide();
            $('#titulo_anomalias').removeClass('parpadea');
            $('#div_anomalias').unbind('mouseover');
            $('#div_anomalias').unbind('mouseleave');
            anomaliasMostradas=false;
            atendiendo = false;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}
function atenderAlerta(idAlerta,aEnviar)
{ 
    
    $.ajax({
        url: "rest/atiendeAlerta.php?idAlerta="+idAlerta+"&aEnviar="+aEnviar,
        type: "post", 
        success: function (alertas) {  
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}
</script>  

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 

</body>
</html>
<?php
// Check if the form is submitted 
$logged = $_REQUEST['logged'];
if( $logged !== "true" )
{ 
    header('Location: login.html');
    die();
}
?>
<!DOCTYPE html> 
<html lang="es">
<head>
<meta charset="UTF-8">
 
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/jquery.min.js"></script>
<link rel="icon" href="img/favicon.png" sizes="32x32" /> 
 
<style>
   html {
    height: 100%;
}
body {
    height: 100%;
    background: rgb(252,252,252);
background: radial-gradient(circle, rgba(252,252,252,1) 0%, rgba(148,187,233,1) 100%);
}
 
.content { 
    position:absolute;
    top:40%;
    width: 100%; 
}
.btn-xl {
    padding: 10px 20px;
    font-size: 2.75em;
    border-radius: 10px;
}
</style>
</head>
   <body > 
   
    <div class="background">
        <div class="content">
            <center><h1 style="text-shadow: 2px 2px 2px black;font-size: 4em;">GUARDIAN</h1>  
             
            <br> 
            <a href="camaras.php?logged=<?php echo $logged ?>"><button type="button" class="btn-xl btn btn-info">ALMACENES</button></a>  
            <div class="btn-group  ">
                <button type="button" class="btn btn-info dropdown-toggle btn-xl" data-toggle="dropdown" aria-expanded="false">
                CAMIONES
                </button>
                <div class="dropdown-menu ">
                    <a class="dropdown-item" href="camiones.php?logged=<?php echo $logged ?>">Histórico Rutas</a>
                    <a class="dropdown-item" href="recomendacionRutaCamiones.php?logged=<?php echo $logged ?>">Ruta recomendada</a> 
                </div>
            </div>
             
            <a href="historicos.php?logged=<?php echo $logged ?>"><button type="button" class="btn-xl btn btn-info">HISTÓRICOS</button></a> 
            <br> <br>
            <a href="login.html" class="btn btn-warning btn-xl">
                <span class="glyphicon glyphicon-log-out"></span> Salir
            </a>
        </center>
        </div>
    </div>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   <script>
       var isMobile = false; 
 if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
 || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
 isMobile = true;
} 

if( isMobile == true )
    {
        $(':button').addClass('btn-xl'); 
        $('.btn-sm').addClass('btn-xl');   
        $('.dropdown-item').addClass('btn-xl');
		$('#select_vehiculo').addClass('btn-xl');  
		$('#select_id_ruta').addClass('btn-xl');  
		$('#div_info').css('font-size','20px');  
		$('#div_anomalias').css('bottom','-20px');
		 
         
        $('#main_image').remove();  

    }
    </script>
   </script>

</body>
</html>
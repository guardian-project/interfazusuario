<?php
$camara = $_GET['camara'];  
$camara = preg_replace('/(^[\"\']|[\"\']$)/', '', $camara);
$logged = $_REQUEST['logged']; 
if( $logged !== "true" )
{ 
    header('Location: login.html');
    die();
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.maphilight.js"></script>  
<script type="text/javascript" src="js/funcionesAccesoCamaras.js?camara=Bitempera 6"></script>
<link rel="stylesheet" href="css/css_camaras.css">
</head>
   <body> 
   <center> 

    <div style="margin-top: 25px;">
     
    <a href="camaras.php?logged=<?php echo $logged ?>" class="btn btn-warning">
        <span class="glyphicon glyphicon-log-out"></span> <b><<</b> VOLVER
    </a>
   
</div> 
    </center> 
 
 
<div style="overflow-y: scroll;" id="div_tabla_valores">  
    <br> 
    <center>
        <table style="padding: 20px 20px 20px 20px;border: solid 1px #000;border-spacing: 5px;border-collapse: separate;" id="tabla_valores">
             
            <tr><td colspan=2 align=center><u><?php echo $camara ?></u></td></tr>
            <tr><td class="label_power" >Potencia activa (kW)</td><td style="text-align: right;"><span id="activepower"></span></td></tr>
            <tr><td class="label_power">Potencia reactiva inductiva (kvarL)</td><td style="text-align: right;"><span id="reactiveinductivepower"></span></td></tr>
            <tr><td class="label_power">Potencia reactiva inductiva (kvarC)</td><td style="text-align: right;"><span id="reactivecapacitivepower"></span></td></tr>
            <tr><td class="label_power">Potencia aparente (kVA)</td><td style="text-align: right;"><span id="apparentpower"></span></td></tr>

            <tr><td colspan=2> </td></tr>

            <tr><td class="label_energy">Energía activa (kWh)</td><td style="text-align: right;"><span id="activeenergy"></span></td></tr>
            <tr><td class="label_energy">Energía reactiva inductiva (kvarLh)</td><td style="text-align: right;"><span id="reactiveinductiveenergy"></span></td></tr>
            <tr><td class="label_energy">Energía reactiva capacitiva (kvarCh)</td><td style="text-align: right;"><span id="reactivecapacitiveenergy"></span></td></tr>
            <tr><td class="label_energy">Energía aparente (kVAh)</td><td style="text-align: right;"><span id="apparentenergy"></span></td></tr>

            <tr><td colspan=2> </td></tr> 
             
             
        </table>
  
</center>
</div> 
<script>
$('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
$(window).on('load', function(){
  setTimeout(removeLoader, 2000); 
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(500, function() { 
      $( "#loadingDiv" ).remove();  
  });  
}
</script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 

   </body>
</html>
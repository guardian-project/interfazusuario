<?php 
$logged = $_REQUEST['logged']; 
if(  $logged !== "true" )
{ 
    header('Location: login.html');
    die();
}
?>
<!DOCTYPE html> 
<html lang="es">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/css_camaras.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery.min.js"></script>
<script src="js/moment.min.js"></script> 
<link rel="icon" href="img/favicon.png" sizes="32x32" /> 
<style>
	body {
		overflow: hidden;
	}
	.map-container {
		height: 100%;
		width: 100%;
		margin-top: 0px;
		z-index: 0;
		display: block !important;
	}
	.btn-xl {
    padding: 10px 20px;
    font-size: 1.75em;
    border-radius: 10px;
    }
    .center{
        position: absolute;
        top: 25%;
        left: 50%;
        transform: translate(-50%, -50%);
		z-index: 999999;
    } 
	.center2{
        position: absolute;
        top: 20%;
        left: 50%;
        transform: translate(-50%, -50%);
		z-index: 999999;
    } 
</style>
 
</head>
   <body>

   <button type="button" id="reset" class="center2 btn btn-secondary"  aria-expanded="false">
        RESET
    </button>

    <div id="div_guia" style="text-align:center;font-weight: bold;border: solid 2px red;border-radius: 10px;background-color: #fff;display:none;width:500px;height:30px;z-index:999999" class="center">
        <span style="color: red;" id="txt_guia">1. Clique sobre el mapa para seleccionar ORIGEN de la ruta&nbsp;&nbsp;&nbsp;</span><span title="Cerrar" style="margin-right: 5px;float: right;cursor: pointer;color: red;" id="cerrar_guia">X</span>
    </div> 
   <center>
    <div style="margin-top: 25px;"> 
	
	<a href="camaras.php?logged=<?php echo $logged ?>"><button type="button" class="btn btn-info">ALMACENES</button></a> 
	 	 
	<div class="btn-group">
        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        CAMIONES
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="camiones.php?logged=<?php echo $logged ?>">Histórico Rutas</a>
            <a class="dropdown-item" href="recomendacionRutaCamiones.php?logged=<?php echo $logged ?>">Ruta recomendada</a> 
        </div>
    </div>
	 

    <a href="historicos.php?logged=<?php echo $logged ?>"><button type="button" class="btn btn-info">HISTÓRICOS</button></a> 
	
	<a href="login.html" class="btn btn-warning">
                <span class="glyphicon glyphicon-log-out"></span> Salir
            </a>
			
    </div>
</center>
 		
		<div class="row" id="div_info" style="padding: 30px;border-radius: 15px 15px 0px 0px;margin: auto;color: #fff;z-index: 99999;position: fixed; bottom: 0px;width: 100%;height: 150px;background-color:rgba(0,0,0,0.6);">
   		
  		 
			<div class="col-2" style="background-color: rgba(0,0,0,0.5);margin: 0px 1px 1px 0px;">
				ORIGEN
			</div>
			<div class="col-8 div_origen" style="background-color: rgba(0,0,0,0.5);margin: 0px 1px 1px 0px;">
				
			</div>  
			<div class="col-2" style="background-color: rgba(0,0,0,0.5);margin: 0px 1px 1px 0px;">
				DESTINO
			</div>
			<div class="col-8 div_destino" style="background-color: rgba(0,0,0,0.5);margin: 0px 1px 1px 0px;">
				
			</div> 
			<div class="col-2" style="background-color: rgba(0,0,0,0.5);margin: 0px 1px 1px 0px;">
				FECHA
			</div>
			<div class="col-8 div_fecha" style="background-color: rgba(0,0,0,0.5);margin: 0px 1px 1px 0px;"> 
			</div>  
	
		</div>
		
		</center>
	 

 
<div class="col-1"></div>
   
	<div class="row">

		<div class="col-12"  style="border: solid 1px #000;height: 100vh;">
			<script>
				var rutas = [];
			</script>
			 
				<div class="map-container"></div>
		</div> 
	</div>
	

	<script>
 
var isMobile = false;
var miRuta = [];
var paso = 1;
var rutaSimulada ={
  "data": [ 
  ]
};
 
$(window).on('load', function(){
  setTimeout(removeLoader, 2000); 
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(500, function() { 
      $( "#loadingDiv" ).remove(); 
  });  
}
 
 if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
 || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
 isMobile = true;
} 

if( isMobile == true )
    { 
        $(':button').addClass('btn-xl');  
        $('.dropdown-item').addClass('btn-xl');
		$('#select_vehiculo').addClass('btn-xl');  
		$('#select_id_ruta').addClass('btn-xl');  
		$('#div_info').css('font-size','20px');  
		$('#div_anomalias').css('bottom','-20px');
		 
         
        $('#main_image').remove();  

		$('#div_guia').css('width', '500px');
		$('#div_guia').css('height', '150px');

    }

	var mapContainer = $('.map-container');
	var map; 
	var directionsService;
	var directionsDisplay; 
	var markersArray = [];
	const directionRenderers = [];
    var rutas=[];
	var bounds; 
	var vehiculoSeleccionado = "";
	function removeDirectionRenderers() {
		directionRenderers.forEach(directionRenderer => {
			directionRenderer.setMap(null);
		});
		directionRenderers.length = 0;
	}

     
    function dibujaMarkersRuta( idRutaSeleccionada)
    {
        if (markersArray) {
            for (i in markersArray) {
            markersArray[i].setMap(null);
            }
        }

        var rutaEnCurso = $.grep(rutas, function (n, i) {
            return (n.ID_RUTA == idRutaSeleccionada );
        }); 
        bounds = new google.maps.LatLngBounds();
		var punto=1;
		var consumo = 0;
		var flow = 0;
		var tiempo = 0;
		var tInicio = 0;
		var tFin = 0;
        $.each( rutaEnCurso[0].waypointsRuta, function( index, waypoint ){
			if( index == 0 )
			{
				tInicio = new Date(waypoint.date); 
			} 
            var location = new google.maps.LatLng(waypoint.latitud,waypoint.longitud); 
            addMarker(idRutaSeleccionada,waypoint,location,punto);
			consumo=parseFloat(consumo)+parseFloat(waypoint.consumo);
			flow=parseInt(flow)+parseInt(waypoint.flow);
			punto++; 
            if( index + 1 == rutaEnCurso[0].waypointsRuta.length )
            {
				tFin = new Date(waypoint.date);   
				var diff  = new Date( tFin - tInicio );
				var tiempo  = diff/1000/60/60;  
                map.fitBounds(bounds); 
				consumo=consumo/rutaEnCurso[0].waypointsRuta.length;
				consumo=consumo.toFixed(2);
				$('.div_consumo').text( consumo ); 
				$('.div_flow').text( flow );
				$('.div_tiempo').text( tiempo.toFixed(2) + " horas" );
            } 
        });     
    }
	function cargaRutasCamion()
	{
		rutas=[];
		$('#select_id_ruta').empty();
		$('#select_id_ruta').prepend( '<option selected>Cargando...</option>' );
		$.ajax({
            url: "rest/consultaRutasCamion.php?vehiculoSeleccionado="+vehiculoSeleccionado,
            type: "post",
            dataType: "json" ,
            success: function (response) {
                var idRutaPrevia=-1;
                var waypointsRuta=[]; 
				var newRuta = false;
                $.each( response, function( index, ruta ){  
                    if( idRutaPrevia != ruta.ID_Ruta )
                    {   
						newRuta = true;

                        if( newRuta == true )
                        { 
							waypointsRuta=[];

							waypointsRuta.push( {"ID_Ruta":ruta.ID_Ruta,"flow":ruta.flow,"consumo":ruta.consumo,"area":ruta.area,"consumo_instantaneo":ruta.consumo_instantaneo,"latitud":ruta.latitud,"longitud":ruta.longitud,"date":ruta.date,"idRegistro":ruta.idRegistro}); 

							if( index > 0 )
							{
								rutas.push({"ID_RUTA": idRutaPrevia , "waypointsRuta": waypointsRuta, "dateInicio": ruta.date }); 
							}

							idRutaPrevia = ruta.ID_Ruta;   
                        } 
                        else
                        { 
                            waypointsRuta.push( {"ID_Ruta":ruta.ID_Ruta,"flow":ruta.flow,"consumo":ruta.consumo,"area":ruta.area,"consumo_instantaneo":ruta.consumo_instantaneo,"latitud":ruta.latitud,"longitud":ruta.longitud,"date":ruta.date,"idRegistro":ruta.idRegistro}); 
                        } 
						 
                    }
                    else
                    { 
						newRuta = false;

						waypointsRuta.push( {"ID_Ruta":idRutaPrevia,"flow":ruta.flow,"consumo":ruta.consumo,"area":ruta.area,"consumo_instantaneo":ruta.consumo_instantaneo,"latitud":ruta.latitud,"longitud":ruta.longitud,"date":ruta.date,"idRegistro":ruta.idRegistro}); 
 				     } 

                    if( index + 1 >= response.length )
                    { 
						$('#select_id_ruta').empty();
                        $.each( rutas, function( indexRuta, ruta ){
							if( ruta.waypointsRuta.length>1)
							{
								var formattedDate = getDateString(new Date(ruta.dateInicio), "d-M-y")
								$('#select_id_ruta').append( '<option value="' + ruta.ID_RUTA + '">' +  formattedDate + '_ruta ' + ruta.ID_RUTA + '</option>' ); 
							}
							if( indexRuta + 1 == rutas.length )
							{
								$('#select_id_ruta').prepend( '<option selected>Seleccionar Ruta</option>' );
							}
                             
                        }); 

                    } 
                });   
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
	}

	var  getDateString = function(date, format) {
        var months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        getPaddedComp = function(comp) {
            return ((parseInt(comp) < 10) ? ('0' + comp) : comp)
        },
        formattedDate = format,
        o = {
            "y+": date.getFullYear(), // year
            "M+": months[date.getMonth()], //month
            "d+": getPaddedComp(date.getDate()), //day
            "h+": getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
             "H+": getPaddedComp(date.getHours()), //hour
            "m+": getPaddedComp(date.getMinutes()), //minute
            "s+": getPaddedComp(date.getSeconds()), //second
            "S+": getPaddedComp(date.getMilliseconds()), //millisecond,
            "b+": (date.getHours() >= 12) ? 'PM' : 'AM'
        };

        for (var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                formattedDate = formattedDate.replace(RegExp.$1, o[k]);
            }
        }
        return formattedDate;
    };

    $( "#cerrar_guia" ).bind( "click", function() {
        $( "#div_guia" ).hide();
	});
	$( "#reset" ).bind( "click", function() {
        location.reload();
	});

	$( document ).ready(function() {
		$('#div_guia').show();
		$('#select_id_ruta').change(function() {  
            dibujaMarkersRuta( $(this).val() );
		});
		$('#select_vehiculo').change(function() {  
            vehiculoSeleccionado = $(this).val();
			cargaRutasCamion();
		});
		$('#select_fecha').change(function() {
			 var fechaSeleccionada = $(this).val(); 
			 var rutasEnFecha = $.grep(rutas, function (n, i) {
				return (n.fecha.substring(0, 10) == fechaSeleccionada.substring(0, 10) );
			});  
			dibujaTodasLasRutas( rutasEnFecha );  
		});  


        
		 
        
	}); 
    var arrSteps=[];

    var directionsServiceFromButton;
    var directionsDisplayFromButton;
    var boundsFromButton;
        
    function initMap() { 


    map = new google.maps.Map(mapContainer[0], {
        zoom: 6,
        scrollwheel:  true,
        center: {lat: 39.8550194, lng: -2.2744792} 
    });
	 
    map.setOptions({styles: styles});
	
	var geocoder = new google.maps.Geocoder();
	map.addListener('click', function(e) {  
		if(paso>2)return false;
		placeMarker(e.latLng, map); 
        var coor = {lat: e.latLng.lat() , long: e.latLng.lng()};
        if( paso == 1)
        {
            miRuta.origenCoordenadas = coor;
        }
        else if( paso == 2)
        {
            miRuta.destinoCoordenadas = coor;
        }
         
         
		geocoder.geocode({
			'latLng': e.latLng
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) { 
                if( paso == 1)
                { 
                    miRuta.origenAddress = results[0].formatted_address;
                    $('.div_origen').text(results[0].formatted_address  );
					 
                    $('#txt_guia').text("2. Clique sobre el mapa para seleccionar DESTINO de la ruta   ");
                    $('#div_guia').show();
                }
                else if( paso == 2)
                { 
                    miRuta.destinoAddress = results[0].formatted_address;
                    var fecha = '<input  onkeydown="return false"  class="fechaRuta" type="date" value="" />'
                    $('.div_destino').text(results[0].formatted_address  ); 
                    $('#txt_guia').text("Seleccione fecha   ");
                    $('#txt_guia').append( fecha );
                    $('#div_guia').show();

                    $('.fechaRuta').change(function() {
                    
                            miRuta.fecha =  $( this ).val();  
                            $('.div_fecha').text($( this ).val());  
                            $('#div_guia').hide();  

							$('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');

							setTimeout(function(){ calculaRuta(); }, 500); 
                    }); 
                    
                } 
                paso=paso+1;
			}
			}
		});
		 
	}); 
  
    function calculaRuta()
    { 
		 
        bounds = new google.maps.LatLngBounds();
        var fecha = moment(miRuta.fecha).startOf('day'); 
        fecha = fecha.toISOString(); 
        var params = {"start": miRuta.origenCoordenadas , "end": miRuta.destinoCoordenadas , "ini": fecha  }; 
		 
        $.ajax({
            url: "https://guardian.odins.es/getRoute", 
            type: "POST",   
			data: JSON.stringify(params), 
			contentType: "application/json",
            async: false,
            error: function(err) {
                console.log("ERROR");
            },
            success: function(dataElements) { 
				removeLoader();
				rutaSimulada.data = dataElements.data;  
                dibujaMarkersRutaSimulada();
            }
        });

    }
	function placeMarker(position, map) {
		var marker = new google.maps.Marker({
			position: position,
			map: map
		});
		 
		map.panTo(position);
	}
    
    directionsServiceFromButton = new google.maps.DirectionsService;
    directionsDisplayFromButton = new google.maps.DirectionsRenderer({
    suppressMarkers: true 
    });
    boundsFromButton = new google.maps.LatLngBounds(); 
		
	var styles = [
		{
		stylers: [
			{ hue: "#e6f2f3" },
			{ saturation: -40 }
		]
		},{
		featureType: "road",
		elementType: "geometry",
		stylers: [
			{ lightness: 100 },
			{ visibility: "simplified" }
		]
		},{
		featureType: "poi",
		elementType: "labels",
		stylers: [
			{ visibility: "off" }
		]
		},{
		featureType: "road",
		elementType: "labels",
		stylers: [
			{ visibility: "simplified" }
		]
		}
		,{
		featureType: "poi.business",
		elementType: "labels",
		stylers: [
			{ visibility: "off" }
		]
		}
	]; 
    var colorRuta = [ "red","#b3b50d","blue","black","orange","white","brown" ];
    var nColor=0; 
    var borraRutas=true;

		
		directionsService = new google.maps.DirectionsService;
			directionsDisplay = new google.maps.DirectionsRenderer({
				suppressMarkers: true

			});  
		} 
		function addMarker(idRutaSeleccionada,waypoint,location,punto) { 
            
			var contentString = "";
			if( isMobile == true )
			{
				contentString =
					'<div id="content" style="font-size: 18px;">' + 
					'<p><center><u><b>Punto </b>' + punto + '</u></center><br>' + 
					'<b>Fecha:</b> ' + waypoint.date + '<br>' + 
					'<b>Latitud:</b> ' + waypoint.latitud + '<br>' + 
					'<b>Longitud:</b> ' + waypoint.longitud + '<br>' + 
					'<b>Flow:</b> ' + waypoint.flow + '<br>' +
					'<b>Consumo instantáneo:</b> ' + waypoint.consumo_instantaneo + 
					"</div>";
			}
			else
			{
				contentString =
					'<div id="content">' + 
					'<p><center><u><b>Punto </b>' + punto + '</u></center><br>' + 
					'<b>Fecha:</b> ' + waypoint.date + '<br>' + 
					'<b>Latitud:</b> ' + waypoint.latitud + '<br>' + 
					'<b>Longitud:</b> ' + waypoint.longitud + '<br>' + 
					'<b>Flow:</b> ' + waypoint.flow + '<br>' +
					'<b>Consumo instantáneo:</b> ' + waypoint.consumo_instantaneo + 
					"</div>";
			}
             

            const infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 200,
            });
			const marker = new google.maps.Marker({
				position: location,
				map: map, 
                icon: 'img/camion.png'
			}); 
            
            marker.addListener("click", () => {
                infowindow.open({
                anchor: marker,
                map,
                shouldFocus: false,
                });
            }); 
            infowindow.open(map,marker);
			markersArray.push(marker);  
			bounds.extend(location); 
		} 

		function dibujaTodasLasRutas( rutasEnFecha )
		{
			var colorRuta = [ "red","yellow","blue","black","orange","white","brown" ];
			$('#div_rutas_en_una_fecha').empty(); 
			$.each( rutasEnFecha, function( index, value ){  
				var boton_ruta = "<input class='boton_ruta inactiva' type='button' name='ruta_" + value.ID_inicioRuta + "' id='ruta_" + value.ID_inicioRuta + "' value='RUTA " + value.ID_inicioRuta + "'>";
				$('#div_rutas_en_una_fecha').append( boton_ruta );  
				 
				if( parseInt(index) + 1 == rutasEnFecha.length )
				{  
					 	
					$( ".boton_ruta" ).bind( "click", function() {
						if( $( this ).hasClass( "inactiva" ) ) 
						{
							var idRuta = $(this).attr("id");
							idRuta= idRuta.replace("ruta_",""); 
							 
							calculateAndDisplayRouteFromButton(idRuta); 
						}
						else  
						{
							var idRuta = $(this).attr("id");
							idRuta= idRuta.replace("ruta_",""); 
							console.log("idRuta -> " + idRuta);  
						} 
						 
					});
					ejecutaRutas(rutasEnFecha);  
				}  
			});
		} 
		 
		function process(elements, cb, timeout) {
			var i = 0;
			var l = elements.length;

			(function fn() {
				cb.call(elements[i++]);
				if (i < l) {
					setTimeout(fn, timeout);
				}
			}());
		}
		 
		function ejecutaRutas(rutasEnFecha)
		{  
			borraRutas=false;
			process($('.boton_ruta'), function() { 
			 
				$(this).click();
				nColor=nColor+1; 
			}, 1000);
			 
		}
		function calculateAndDisplayRouteFromButton(idRutaSeleccionada,borra) {
			 
			directionsDisplay = new google.maps.DirectionsRenderer();
			directionsDisplay.setMap(map);

			
			var rutaEnCurso = $.grep(rutas, function (n, i) {
				return (n.ID_inicioRuta == idRutaSeleccionada );
			});  
			
			var start = new google.maps.LatLng(rutaEnCurso[0].latitudInicio,rutaEnCurso[0].longitudInicio); 
			var end = new google.maps.LatLng(rutaEnCurso[0].latitudFin,rutaEnCurso[0].longitudFin);  
			 
			addMarker(start,"Inicio Ruta " + idRutaSeleccionada);
			addMarker(end,"Fin Ruta " + idRutaSeleccionada);  

			var request = {
				origin: start,
				destination: end,
				travelMode: google.maps.TravelMode.DRIVING
			};
			directionsDisplay.setOptions({
				suppressMarkers: true,
				polylineOptions: {
				strokeColor: colorRuta[nColor],
				strokeWeight: 5,
				idRuta: idRutaSeleccionada
				}
			});

			$('#ruta_'+idRutaSeleccionada).css('background-color',colorRuta[nColor]);
			$('#ruta_'+idRutaSeleccionada).css('color','#fff');
			$('#ruta_'+idRutaSeleccionada).removeClass('inactiva');
			$('#ruta_'+idRutaSeleccionada).addClass('activa');
		 
			

			var rutaEnCurso = $.grep(rutas, function (n, i) {
				return (n.ID_inicioRuta == idRutaSeleccionada );
			});  
			
			var start = new google.maps.LatLng(rutaEnCurso[0].latitudInicio,rutaEnCurso[0].longitudInicio); 
			var end = new google.maps.LatLng(rutaEnCurso[0].latitudFin,rutaEnCurso[0].longitudFin);
		 
			bounds = new google.maps.LatLngBounds();
			bounds.extend(start);
			bounds.extend(end);
			map.fitBounds(bounds);
			var request = {
				origin: start,
				destination: end,
				travelMode: google.maps.TravelMode.DRIVING
			};
			directionsService.route(request, function (response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					directionsDisplay.setMap(map);
				} else {
					alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
				}
			});
			
		}
		function calculateAndDisplayRoute(directionsService, directionsDisplay,map,idRutaSeleccionada) {
			 
			 directionsDisplay.setMap(null); 
			
			var rutaEnCurso = $.grep(rutas, function (n, i) {
				return (n.ID_inicioRuta == idRutaSeleccionada );
			});  
			
			var start = new google.maps.LatLng(rutaEnCurso[0].latitudInicio,rutaEnCurso[0].longitudInicio); 
			var end = new google.maps.LatLng(rutaEnCurso[0].latitudFin,rutaEnCurso[0].longitudFin);
 
			if (markersArray) {
				for (i in markersArray) {
				markersArray[i].setMap(null);
				}
				markersArray.length = 0;
			} 
			 
			addMarker(start,"Inicio");
			addMarker(end,"Fin"); 
			
			var bounds = new google.maps.LatLngBounds();
			bounds.extend(start);
			bounds.extend(end);
			map.fitBounds(bounds);
			var request = {
				origin: start,
				destination: end,
				travelMode: google.maps.TravelMode.DRIVING
			};
			directionsService.route(request, function (response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					directionsDisplay.setMap(map);
				} else {
					alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
				}
			}); 
		}



        function calculaRuta1()
    {
		 

        bounds = new google.maps.LatLngBounds();
        var fecha = moment(miRuta.fecha).startOf('day'); 
        fecha = fecha.toISOString(); 
        var params = {"start": {lat:38.792709615342325,long:-6.53717451249999} , "end": {lat:38.792709615342325,long:-6.53717451249999} , "ini": "2022-02-08T23:00:00.000Z" };
        $.ajax({
            url: "simulaRuta.php",
            type: "GET",  
            data: jQuery.param(params), 
            async: false,
            error: function(err) {
                console.log("ERROR");
            },
            success: function(dataElements) {
                 
                dibujaMarkersRutaSimulada();
            }
        }); 
    }
    


function dibujaMarkersRutaSimulada()
    {
        if (markersArray) {
            for (i in markersArray) {
            markersArray[i].setMap(null);
            }
        } 
		var punto=1; 
        $.each( rutaSimulada.data, function( index, waypoint ){ 
            var location = new google.maps.LatLng(waypoint[1],waypoint[0]);   
            var contentString = "";
			if( isMobile == true )
			{
				contentString =
					'<div id="content" style="font-size: 18px;">' + 
					'<p><center><u><b>Punto </b>' + punto + '</u></center><br>' +  
					'<b>Latitud:</b> ' + waypoint[1] + '<br>' + 
					'<b>Longitud:</b> ' + waypoint[0] 
					"</div>";
			}
			else
			{
				contentString =
					'<div id="content">' + 
					'<p><center><u><b>Punto </b>' + punto + '</u></center><br>' +  
					'<b>Latitud:</b> ' + waypoint[1] + '<br>' + 
					'<b>Longitud:</b> ' + waypoint[0]  
					"</div>";
			}
             

            const infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 200,
            });
			const marker = new google.maps.Marker({
				position: location,
				map: map, 
                icon: 'img/camion.png'
			}); 
            
            marker.addListener("click", () => {
                infowindow.open({
                anchor: marker,
                map,
                shouldFocus: false,
                });
            }); 
            infowindow.open(map,marker);
			markersArray.push(marker);  
			 
             punto = punto + 1;

             bounds.extend(location); 
            map.fitBounds(bounds);
            
        });     
    } 
	</script>
       
	 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTO9QwDnPVklMjLd6ajJZvYFloJRmDGOM&callback=initMap"
        async defer></script>

   </body>
</html>
<?php 
$logged = $_REQUEST['logged']; 
if( $logged !== "true" )
{ 
    header('Location: login.html');
    die();
}
?>
<!DOCTYPE html> 
<html lang="es">
<head>
<meta charset="UTF-8">
 
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/css_camaras.css">
<link rel="icon" href="img/favicon.png" sizes="32x32" /> 
<script src="js/plotly-2.8.3.min.js"></script>

<script src="js/jquery.min.js"></script>
<script src="js/moment.min.js"></script> 
<script src="xls-export.js"></script>

<style>
	body {
		overflow: hidden;
	}
	.map-container {
		height: 100%;
		width: 100%;
		margin-top: 0px;
		z-index: 0;
		display: block !important;
	}
	.btn-xl {
    padding: 10px 20px;
    font-size: 1.75em;
    border-radius: 10px;
} 
</style>
</head>
   <body> 

    <div id="div_excel" name="div_excel" style="position:fixed;bottom: 100px;left: 100px;z-index: 99999;display:none;"> 
        <img style="width: 60px;cursor: pointer;" src="img/excel.png">
    </div>
    <center> 

    <div style="margin-top: 25px;">

    <a href="camaras.php?logged=<?php echo $logged ?>"><button type="button" class="btn btn-info">ALMACENES</button></a> 
	
    <div class="btn-group  ">
                <button type="button" class="btn btn-info dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                CAMIONES
                </button>
                <div class="dropdown-menu ">
                    <a class="dropdown-item" href="camiones.php?logged=<?php echo $logged ?>">Histórico Rutas</a>
                    <a class="dropdown-item" href="recomendacionRutaCamiones.php?logged=<?php echo $logged ?>">Ruta recomendada</a> 
                </div>
            </div> 
    <a href="#"><button disabled type="button" class="btn btn-secondary">HISTÓRICOS</button></a> 
 
    <a href="login.html" class="btn btn-warning">
                <span class="glyphicon glyphicon-log-out"></span> Salir
            </a>
    </div> 
    </center> 
  
    <div id="graficas"  name="graficas" class="absoluteCenter"  style="width:600px;height:250px;z-index:-1"></div>  
    
    <center> 
        <div class="centerW" id="panelDown"   name="panelDown" style="top:90%;margin: 10px 10px 10px 10px;padding: 7px 20px 10px 20px;position: relative"> 
         
        
        <a class="btn btn-sm btn-secondary btn_collapse_consumos"  onclick="tipo('consumos')" data-toggle="collapse" href="#consumos" role="button" aria-expanded="false" aria-controls="consumos">
            CONSUMOS
        </a>
        <a class="btn btn-sm btn-secondary btn_collapse_temperaturas" onclick="tipo('temperaturas')" data-toggle="collapse" href="#temperaturas" role="button" aria-expanded="false" aria-controls="temperaturas">
            TEMPERATURAS
        </a>

        <a class="btn btn-sm btn-secondary btn_collapse_humedades" onclick="tipo('humedades')" data-toggle="collapse" href="#humedades" role="button" aria-expanded="false" aria-controls="humedades">
            HUMEDADES
        </a>
        <a class="btn btn-sm btn-secondary btn_collapse_CO2" onclick="tipo('CO2')" data-toggle="collapse" href="#CO2" role="button" aria-expanded="false" aria-controls="CO2">
            CO2
        </a>
        <a class="btn btn-sm btn-secondary btn_collapse_PUERTAS" onclick="tipo('PUERTAS')" data-toggle="collapse" href="#PUERTAS" role="button" aria-expanded="false" aria-controls="PUERTAS">
            PUERTAS
        </a>
         
        
        <div style="margin: 10px;"  id="consumos" class="collapse row">
         
                <div class="col-9" style="background-color: rgba(239,239,239,0.8);border-radius: 8px;border: solid 1px #000;"> 

                <h5>Seleccionar DISPOSITIVO/S</h5>
                <div class="row"><hr></div>

                    <div class="row">
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" entity="ConsumosPorCamaras" attribute="analogInput_61551006c6b8ec8f210ecc5d" class="form-check-input" type="checkbox" value="" id="id_analogInput_61551006c6b8ec8f210ecc5d">
                                <label class="form-check-label" for="id_analogInput_61551006c6b8ec8f210ecc5d">
                                Consumo Congelados
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="ConsumosPorCamaras"  attribute="analogInput_61551006c6b8ec4d200ecc75"  type="checkbox" value="" id="id_analogInput_61551006c6b8ec4d200ecc75">
                                <label class="form-check-label" for="id_analogInput_61551006c6b8ec4d200ecc75">
                                Consumo Frescos 2
                                </label>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="ConsumosPorCamaras"  attribute="analogInput_61551006c6b8ecd7120ecc8d"  type="checkbox" value="" id="id_analogInput_61551006c6b8ecd7120ecc8d">
                                <label class="form-check-label" for="id_analogInput_61551006c6b8ecd7120ecc8d">
                                Consumo Bitempera 4
                                </label>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="ConsumosPorCamaras"  attribute="analogInput_61551007c6b8ec203b0ecca5"  type="checkbox" value="" id="id_analogInput_61551007c6b8ec203b0ecca5">
                                <label class="form-check-label" for="id_analogInput_61551007c6b8ec203b0ecca5">
                                Consumo Bitempera 5
                                </label>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="ConsumosPorCamaras"  attribute="analogInput_61551007c6b8ec56240eccbd"  type="checkbox" value="" id="id_analogInput_61551007c6b8ec56240eccbd">
                                <label class="form-check-label" for="id_analogInput_61551007c6b8ec56240eccbd">
                                Consumo Bitempera 6
                                </label>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="IoTConnector:00027"  attribute="analogInput_614c6767bc1aef40ce621780"  type="checkbox" value="" id="id_analogInput_614c6767bc1aef40ce621780">
                                <label class="form-check-label" for="id_analogInput_614c6767bc1aef40ce621780">
                                Consumo Global Cuadro Interior (Sala de máquinas)
                                </label>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="IoTConnector:00027"  attribute="analogInput_614c6768bc1aef4a9462181c"  type="checkbox" value="" id="id_analogInput_614c6768bc1aef4a9462181c">
                                <label class="form-check-label" for="id_analogInput_614c6768bc1aef4a9462181c">
                                Consumo Global Cuadro Central (Sala de máquinas)
                                </label>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="IoTConnector:00027"  attribute="analogInput_614c6768bc1aeff6a96218ee"  type="checkbox" value="" id="id_analogInput_614c6768bc1aeff6a96218ee">
                                <label class="form-check-label" for="id_analogInput_614c6768bc1aeff6a96218ee">
                                Consumo Global Cuadro Exterior (Sala de máquinas)
                                </label>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="bar" class="form-check-input"  entity="ConsumosPorCamaras"  attribute="analogInput_61af7c4a37c704101927b213"  type="checkbox" value="" id="id_analogInput_61af7c4a37c704101927b213">
                                <label class="form-check-label" for="id_analogInput_61af7c4a37c704101927b213">
                                Consumo Muelle
                                </label>
                            </div> 
                        </div>
                        
                    </div>
                        
                    
                </div>
                 
            </div>
        </div> 

        <div style="margin: 0px 10px 10px 10px;" id="temperaturas" class="collapse row">
                <div class="col-9" style="background-color: rgba(239,239,239,0.8);border-radius: 8px;border: solid 1px #000;"> 

                <h5>Seleccionar DISPOSITIVO/S</h5>
                <div class="row"><hr></div>

                    <div class="row">
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c0f3a2f16c91" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c0f3a2f16c91">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c0f3a2f16c91">
                                Temperatura 1 (centro)
                                </label>
                            </div>
                        </div> 
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="MG0101E20018500010" attribute="analogInput_614c61bebc1aef45e36216a5" class="form-check-input" type="checkbox" value="" id="id_analogInput_614c61bebc1aef45e36216a5">
                                <label class="form-check-label" for="id_analogInput_614c61bebc1aef45e36216a5">
                                Muelle a Frescos 2
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="MG0101E20018500011" attribute="analogInput_614c61dbbc1aef79756216b3" class="form-check-input" type="checkbox" value="" id="id_analogInput_614c61dbbc1aef79756216b3">
                                <label class="form-check-label" for="id_analogInput_614c61dbbc1aef79756216b3">
                                Muelle a Frescos 3
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="MD0101E20010700093" attribute="analogInput_61a672ad37c70439bd27b1f7" class="form-check-input" type="checkbox" value="" id="id_analogInput_61a672ad37c70439bd27b1f7">
                                <label class="form-check-label" for="id_analogInput_61a672ad37c70439bd27b1f7">
                                Exterior
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c0f3a2f16c91" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c0f3a2f16c91">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c0f3a2f16c91">
                                Congelados
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c0366bf16c94" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c0366bf16c94">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c0366bf16c94">
                                Frescos 2
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c0172ef16c97" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c0172ef16c97">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c0172ef16c97">
                                Frescos 3.1
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_61a75f8537c7048cb827b200" class="form-check-input" type="checkbox" value="" id="id_analogInput_61a75f8537c7048cb827b200">
                                <label class="form-check-label" for="id_analogInput_61a75f8537c7048cb827b200">
                                Frescos 3.2
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c047f6f16c9a" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c047f6f16c9a">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c047f6f16c9a">
                                Frescos 7
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c0e679f16c9d" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c0e679f16c9d">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c0e679f16c9d">
                                Bitempera 4
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c068fff16ca0" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c068fff16ca0">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c068fff16ca0">
                                Bitempera 5
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input  modeGrpah="lines" entity="IoTConnector:00027" attribute="analogInput_614cc3b98562c084ebf16ca3" class="form-check-input" type="checkbox" value="" id="id_analogInput_614cc3b98562c084ebf16ca3">
                                <label class="form-check-label" for="id_analogInput_614cc3b98562c084ebf16ca3">
                                Bitempera 6
                                </label>
                            </div>
                        </div>
                       
                    </div>
                        
                    
                </div>
                
            </div>
        </div>  

        <div style="margin: 0px 10px 10px 10px;" id="humedades" class="collapse row">
                <div class="col-9" style="background-color: rgba(239,239,239,0.8);border-radius: 8px;border: solid 1px #000;"> 

                <h5>Seleccionar DISPOSITIVO/S</h5>
                <div class="row"><hr></div>

                    <div class="row">
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="lines" entity="MD0101E20010700093" attribute="analogInput_61a672ad37c704f25927b1fa" class="form-check-input" type="checkbox" value="" id="id_analogInput_61a672ad37c704f25927b1fa">
                                <label class="form-check-label" for="id_analogInput_61a672ad37c704f25927b1fa">
                                Exterior
                                </label>
                            </div>
                        </div> 
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="lines" entity="MG0101E20018500010" attribute="analogInput_614c61bebc1aef1fad6216a8" class="form-check-input" type="checkbox" value="" id="id_analogInput_614c61bebc1aef1fad6216a8">
                                <label class="form-check-label" for="id_analogInput_614c61bebc1aef1fad6216a8">
                                Muelle a Frescos 2
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="lines" entity="MG0101E20018500011" attribute="analogInput_614c61dbbc1aef00a66216b6" class="form-check-input" type="checkbox" value="" id="id_analogInput_614c61dbbc1aef00a66216b6">
                                <label class="form-check-label" for="id_analogInput_614c61dbbc1aef00a66216b6">
                                Muelle a Frescos 3
                                </label>
                            </div>
                        </div>  
                        
                    </div>
                        
                    
                </div>
                
            </div>
        </div> 

        <div style="margin: 0px 10px 10px 10px;" id="CO2" class="collapse row">
                <div class="col-9" style="background-color: rgba(239,239,239,0.8);border-radius: 8px;border: solid 1px #000;"> 

                <h5>Seleccionar DISPOSITIVO/S</h5>
                <div class="row"><hr></div>

                    <div class="row">
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="lines" entity="MG0101E20018500010" attribute="analogInput_614c61bebc1aef58366216ab" class="form-check-input" type="checkbox" value="" id="id_analogInput_614c61bebc1aef58366216ab">
                                <label class="form-check-label" for="id_analogInput_614c61bebc1aef58366216ab">
                                Muelle a Frescos 2
                                </label>
                            </div>
                        </div> 
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="lines" entity="MG0101E20018500011" attribute="analogInput_614c61dbbc1aefd06a6216b9" class="form-check-input" type="checkbox" value="" id="id_analogInput_614c61dbbc1aefd06a6216b9">
                                <label class="form-check-label" for="id_analogInput_614c61dbbc1aefd06a6216b9">
                                Muelle a Frescos 3
                                </label>
                            </div>
                        </div> 
                         
                    </div>
                        
                    
                </div>
                
            </div>
        </div> 

        <div style="margin: 0px 10px 10px 10px;" id="PUERTAS" class="collapse row">
                <div class="col-9" style="background-color: rgba(239,239,239,0.8);border-radius: 8px;border: solid 1px #000;"> 

                <h5>Seleccionar DISPOSITIVO/S</h5>
                <div class="row"><hr></div>

                    <div class="row">
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c05cfcf16cb8" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c05cfcf16cb8">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c05cfcf16cb8">
                                Muelle 1
                                </label>
                            </div>
                        </div> 
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c0553df16cbb" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c0553df16cbb">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c0553df16cbb">
                                Muelle 2
                                </label>
                            </div>
                        </div> 
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c0aba3f16cbe" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c0aba3f16cbe">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c0aba3f16cbe">
                                Muelle 3
                                </label>
                            </div>
                        </div> 
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c00973f16cc1" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c00973f16cc1">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c00973f16cc1">
                                Muelle 4
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c09d96f16cc4" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c09d96f16cc4">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c09d96f16cc4">
                                Muelle 5
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c0785cf16cc7" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c0785cf16cc7">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c0785cf16cc7">
                                Muelle 6
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c03a19f16cca" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c03a19f16cca">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c03a19f16cca">
                                Muelle 7
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c09bcdf16ccd" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c09bcdf16ccd">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c09bcdf16ccd">
                                Muelle 8
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c099a5f16cd0" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c099a5f16cd0">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c099a5f16cd0">
                                Salida lateral Muelle
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c00ce1f16cd3" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c00ce1f16cd3">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c00ce1f16cd3">
                                Muelle a Frescos 2
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00028" attribute="digitalInput_614cc7868562c05674f16cd6" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc7868562c05674f16cd6">
                                <label class="form-check-label" for="id_digitalInput_614cc7868562c05674f16cd6">
                                Muelle a Frescos 3
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00027" attribute="digitalInput_614cc3e98562c065f3f16caf" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc3e98562c065f3f16caf">
                                <label class="form-check-label" for="id_digitalInput_614cc3e98562c065f3f16caf">
                                Frescos 2 a Congelados
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00027" attribute="digitalInput_614cc3e98562c0d804f16cac" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc3e98562c0d804f16cac">
                                <label class="form-check-label" for="id_digitalInput_614cc3e98562c0d804f16cac">
                                Frescos 2 a Frescos 7
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00027" attribute="digitalInput_614cc3e98562c06e6cf16ca6" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc3e98562c06e6cf16ca6">
                                <label class="form-check-label" for="id_digitalInput_614cc3e98562c06e6cf16ca6">
                                Frescos 3 a Frescos 7
                                </label>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-check">
                                <input modeGrpah="bar" entity="IoTConnector:00027" attribute="digitalInput_614cc3e98562c007eaf16ca9" class="form-check-input" type="checkbox" value="" id="id_digitalInput_614cc3e98562c007eaf16ca9">
                                <label class="form-check-label" for="id_digitalInput_614cc3e98562c007eaf16ca9">
                                Frescos 7 a Bitempera 4
                                </label>
                            </div>
                        </div>
                        
                    </div>
                        
                    
                </div>
               
            </div>
        </div> 

        

        
    </center> 
    

    <script>

var isMobile = false;
 // device detection
 if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
 || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
 isMobile = true;
} 

if( isMobile == true )
    {
        $(':button').addClass('btn-xl'); 
        $('.btn-sm').addClass('btn-xl');   
        $('.dropdown-item').addClass('btn-xl');
		$('#select_vehiculo').addClass('btn-xl');  
		$('#select_id_ruta').addClass('btn-xl');  
		$('#div_info').css('font-size','20px');  
		$('#div_anomalias').css('bottom','-20px');
		 
         
        $('#main_image').remove();  

    }

var miToken=""; 
var dispositivosAdded = [];
var totalDispositivos = []; 
var tipoActivo = ""; 
var url = ""

        var width = 0;
        var height = 0;
        $( document ).ready(function() { 
            
        }); 
    
         
     
    $(':checkbox').change(function() {
        if(this.checked) { 


            var entity = $(this).attr('entity');
            var attribute = $(this).attr('attribute'); 
            var modeGrpah = $(this).attr('modeGrpah'); 
            var dispositivo =  jQuery('label[for=' + jQuery(this).attr('id') + ']').html();
            var nombre = jQuery('label[for=' + jQuery(this).attr('id') + ']').html(); 

            dispositivo = dispositivo.replace(/\ /g, '');
            dispositivo = dispositivo.replace(/\n/g, '');   

            var arr = { dispositivo: jQuery(this).attr('id') , nombre: dispositivo , entity: entity , attribute: attribute  , modeGrpah: modeGrpah };
            dispositivosAdded.push( arr ); 
            
            actualizaGrafica(entity,attribute,dispositivo,modeGrpah);  
        } 
        else
        { 
            var dispositivo =  jQuery('label[for=' + jQuery(this).attr('id') + ']').html();
            dispositivo = dispositivo.replace(/\ /g, '');
            dispositivo = dispositivo.replace(/\n/g, '');   

            var index; 
            graph.data.some(function(entry, i) {
                if (entry.name === dispositivo) {
                    index = i;
                    return true;
                }
            });

            dispositivosAdded = jQuery.grep(dispositivosAdded, function( a ) {
                return a.nombre != dispositivo;
            });  
            
            
            deleteTrace(index);

        }     
    }); 

    function deleteTrace(index){
        Plotly.deleteTraces(graph, index);

        totalDispositivos = graph.data; 
                    
        if( totalDispositivos.length>0)
        {
            console.log("totalDispositivos");
            console.log(totalDispositivos);

            $('#div_excel').show();
        }
        else
        {
            $('#div_excel').hide(); 
        }
        console.log("totalDispositivos.length");
            console.log(totalDispositivos.length);
    };

    function tipo(tipo)
    {
        tipoActivo = tipo; 
        if( $('#'+tipoActivo).is( ":visible" ) == false )//CERRAR CUALQUIER OTRA ABIERTA
        {
            if( "#consumos" != '#'+tipoActivo )$('#consumos').collapse('hide');
            if( "#temperaturas" != '#'+tipoActivo )$('#temperaturas').collapse('hide'); 
            if( "#humedades" != '#'+tipoActivo )$('#humedades').collapse('hide');
            if( "#CO2" != '#'+tipoActivo )$('#CO2').collapse('hide');
            if( "#PUERTAS" != '#'+tipoActivo )$('#PUERTAS').collapse('hide');

            if( "#consumos" != '#'+tipoActivo )$('.btn_collapse_consumos').removeClass('btn-success');
            if( "#consumos" != '#'+tipoActivo )$('.btn_collapse_consumos').addClass('btn-secondary');
            if( "#temperaturas" != '#'+tipoActivo )$('.btn_collapse_temperaturas').removeClass('btn-success');
            if( "#temperaturas" != '#'+tipoActivo )$('.btn_collapse_temperaturas').addClass('btn-secondary');
            if( "#humedades" != '#'+tipoActivo )$('.btn_collapse_humedades').removeClass('btn-success');
            if( "#humedades" != '#'+tipoActivo )$('.btn_collapse_humedades').addClass('btn-secondary');
            if( "#CO2" != '#'+tipoActivo )$('.btn_collapse_CO2').removeClass('btn-success');
            if( "#CO2" != '#'+tipoActivo )$('.btn_collapse_CO2').addClass('btn-secondary');
            if( "#PUERTAS" != '#'+tipoActivo )$('.btn_collapse_PUERTAS').removeClass('btn-success');
            if( "#PUERTAS" != '#'+tipoActivo )$('.btn_collapse_PUERTAS').addClass('btn-secondary');

            if( "#consumos" === '#'+tipoActivo )$('.btn_collapse_consumos').removeClass('btn-secondary');
            if( "#consumos" === '#'+tipoActivo )$('.btn_collapse_consumos').addClass('btn-success');
            if( "#temperaturas" === '#'+tipoActivo )$('.btn_collapse_temperaturas').removeClass('btn-secondary');
            if( "#temperaturas" === '#'+tipoActivo )$('.btn_collapse_temperaturas').addClass('btn-success');
            if( "#humedades" === '#'+tipoActivo )$('.btn_collapse_humedades').removeClass('btn-secondary');
            if( "#humedades" === '#'+tipoActivo )$('.btn_collapse_humedades').addClass('btn-success');
            if( "#CO2" === '#'+tipoActivo )$('.btn_collapse_CO2').removeClass('btn-secondary');
            if( "#CO2" === '#'+tipoActivo )$('.btn_collapse_CO2').addClass('btn-success');
            if( "#PUERTAS" === '#'+tipoActivo )$('.btn_collapse_PUERTAS').removeClass('btn-secondary');
            if( "#PUERTAS" === '#'+tipoActivo )$('.btn_collapse_PUERTAS').addClass('btn-success');

            $( "#fechas") .remove();

             
                 


            var fechas = '<div id="fechas" class="col-3" style="background-color: rgba(239,239,239,0.8);border-radius: 8px;border: solid 1px #000;"><h5>Seleccionar FECHAS</h5><div class="row"><hr></div><label>Inicio&nbsp;</label>';
            fechas = fechas + '<input onkeydown="return false" class="fInicio" type="date" value="<?php echo date('Y-m-d'); ?>" /><br><label>Fin&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
            fechas = fechas + '<input  onkeydown="return false" class="fFin" type="date" value="<?php echo date('Y-m-d'); ?>" /></div>'; 
                 
            $( "#" + tipoActivo ).append( fechas ); 

            if( dispositivosAdded.fechaInicio != undefined)$('.fInicio').val( dispositivosAdded.fechaInicio );
            if( dispositivosAdded.fechaFin != undefined)$('.fFin').val( dispositivosAdded.fechaFin ); 
            $('.fInicio').change(function() {
                    
                    dispositivosAdded.fechaInicio =  $( this ).val();   
                    if(graph.data != "undefined" && graph.data != undefined)
                    {
                        while(graph.data.length>0)
                        {
                            Plotly.deleteTraces(graph, [0]);
                        }   
                        setTimeout(function(){
                            $.each(dispositivosAdded, function (ind, elem) { 
                                actualizaGrafica(elem.entity,elem.attribute,elem.nombre,elem.modeGrpah);   
                            });  
                        }, 500);
                    } 
                    
                        
            }); 
            $('.fFin').change(function() {
                 
                    dispositivosAdded.fechaFin =  $( this ).val();
                    if(graph.data != "undefined" && graph.data != undefined)
                    {
                        while(graph.data.length>0)
                        {
                            Plotly.deleteTraces(graph, [0]);
                        }  
                        setTimeout(function(){
                            $.each(dispositivosAdded, function (ind, elem) { 
                                actualizaGrafica(elem.entity,elem.attribute,elem.nombre,elem.modeGrpah);   
                            });  
                        }, 500); 
                    }
                     
            });   
              
        }
        else{
            if( "#consumos" === '#'+tipoActivo )$('.btn_collapse_consumos').removeClass('btn-success');
            if( "#consumos" === '#'+tipoActivo )$('.btn_collapse_consumos').addClass('btn-secondary');
            if( "#temperaturas" === '#'+tipoActivo )$('.btn_collapse_temperaturas').removeClass('btn-success');
            if( "#temperaturas" === '#'+tipoActivo )$('.btn_collapse_temperaturas').addClass('btn-secondary');
            if( "#humedades" === '#'+tipoActivo )$('.btn_collapse_humedades').removeClass('btn-success');
            if( "#humedades" === '#'+tipoActivo )$('.btn_collapse_humedades').addClass('btn-secondary');
            if( "#CO2" === '#'+tipoActivo )$('.btn_collapse_CO2').removeClass('btn-success');
            if( "#CO2" === '#'+tipoActivo )$('.btn_collapse_CO2').addClass('btn-secondary');
            if( "#PUERTAS" === '#'+tipoActivo )$('.btn_collapse_PUERTAS').removeClass('btn-success');
            if( "#PUERTAS" === '#'+tipoActivo )$('.btn_collapse_PUERTAS').addClass('btn-secondary');

            $( "#fechas") .remove();
 
        } 
    }
    var graph = document.getElementById('graficas');
    var layout = { 
    xaxis: { 
        showgrid: false,
        zeroline: false
    },
    type: 'bar',
    yaxis: { 
        showline: false
    } ,
    showlegend: true
    }; 
    var nTraces = 0;
            function actualizaGrafica( entity,attribute,dispositivo,modeGrpah )
            {  
            var dateInicio = dispositivosAdded.fechaInicio;  
            var dateFin = new Date(dispositivosAdded.fechaFin);  
            
            dateFin   = moment(dispositivosAdded.fechaFin).endOf('day');
            dateFin = dateFin.toISOString(); 
            dateInicio = moment(dispositivosAdded.fechaInicio).startOf('day');//dispositivosAdded.fechaInicio; // moment(dateInicio).startOf('day');//moment( dateInicio ).toISOString(); 
            dateInicio = dateInicio.toISOString(); 
            $.ajax({
            url: "",
            type: "GET",  
            dataType: 'json',
            headers: {"x-access-token": miToken}, 
            async: false,
            error: function(err) {
                console.log("ERROR");
            },
            success: function(datos) {
                var valores = datos.contextResponses[0].contextElement.attributes[0].values; 
   

                if( nTraces == 0 )
                {
                    let trace  = {
                        x: [],
                        y: [],
                        type: modeGrpah ,
                        name: dispositivo
                    };

                    valores.forEach(function(val) {
                        trace.x.push(val["recvTime"]);
                        trace.y.push(val["attrValue"]); 
                    }); 
                    Plotly.newPlot(graph, [trace], layout);
                     
                    totalDispositivos = graph.data; 
                    
                    if( totalDispositivos.length>0)
                    {
                        console.log("totalDispositivos");
                        console.log(totalDispositivos);

                        $('#div_excel').show();
                    }
                    else
                    {
                        $('#div_excel').hide(); 
                    }
                }
                else
                { 
                    let trace  = {
                        x: [],
                        y: [],
                        type: modeGrpah,
                        name: dispositivo
                    };
                    
                    valores.forEach(function(val) {
                        trace.x.push(val["recvTime"]);
                        trace.y.push(val["attrValue"]); 
                    }); 
                    Plotly.addTraces(graph, [trace], 0); 
                     
                    totalDispositivos = graph.data; 
                     
                    if( totalDispositivos.length>0)
                    {
                        console.log("totalDispositivos");
                        console.log(totalDispositivos);
                        $('#div_excel').show();
                    }
                    else
                    {
                        $('#div_excel').hide(); 
                    }
                } 
            }
        });
        nTraces=nTraces+1;
    }

    document.querySelector('#div_excel').addEventListener('click', function() { 
        
        var datosExport = [];
        var nombre = "";
         
        totalDispositivos.forEach(function(valor) {
            nombre = valor.name;
            console.log(valor);
            var index = 0;
            valor.x.forEach(function(valX) {  
                var arr = {fecha:valX,valor:valor.y[index] };
                datosExport.push(arr);
                index = index+1;
            });  
             
            const xls = new XlsExport(datosExport, nombre);
            xls.exportToCSV(); 
        }); 
         
         
         
    })

    </script>
 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 
</body>
</html>